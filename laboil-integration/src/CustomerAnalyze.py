import helpers
from Analyze import Analyze
import json

class CustomerAnalyze(object): 
      
    def __init__(self, values_dict):


        self.CustomerShortname: str
        self.CustomerName: str
        self.Analyzes = []

        helpers.set_class_property(self, "CustomerShortname", values_dict, True)
        helpers.set_class_property(self, "CustomerName", values_dict, True)
                
        
        for analyze_dict in values_dict["analyzes"]:
            analyze = Analyze(analyze_dict)
            self.Analyzes.append(analyze)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=False, indent=4)
        
