import helpers
from Analyze import Analyze

def getMap(analyze: Analyze):
    return {    
        #Chemistry
        "SKFCM_ASCT_Molybdenum": analyze.Chemistry.Molybdenum,
        "SKFCM_ASCT_Calcium":  analyze.Chemistry.Calcium,
        "SKFCM_ASCT_Magnesium":  analyze.Chemistry.Magnesium,
        "SKFCM_ASCT_Phosphorous":  analyze.Chemistry.Phosphor,
        "SKFCM_ASCT_Zinc":  analyze.Chemistry.Zinc,
        "SKFCM_ASCT_Barium":  analyze.Chemistry.Barium,
        "SKFCM_ASCT_Boron":  analyze.Chemistry.Boron,
        "SKFCM_ASCT_TAN":  analyze.Chemistry.Tan,
        "SKFCM_ASCT_Oxidation":  analyze.Chemistry.Oxidation,
        "SKFCM_ASCT_ViscosityAt40c":  analyze.Chemistry.Viscosity40,
        # "FluidIntegrity":  analyze.Chemistry.FluidIntegrity,

        #Contamination,
        "SKFCM_ASCT_Sodium":  analyze.Contamination.Sodium,
        # "Vanadium":  analyze.Contamination.Vanadium,
        "SKFCM_ASCT_Potassium":  analyze.Contamination.Potassium,
        # "SKFCM_ASCT_LITHIUM":  analyze.Contamination.Lithium, <---------
        "SKFCM_ASCT_ISO04":  analyze.Contamination.CodeIso4406HigherThan4,
        "SKFCM_ASCT_ISO06":  analyze.Contamination.CodeIso4406HigherThan6,
        "SKFCM_ASCT_ISO14":  analyze.Contamination.CodeIso4406HigherThan14,
        # "CntsHigherThan4":  analyze.Contamination.CntsHigherThan4,
        # "CntsHigherThan6":  analyze.Contamination.CntsHigherThan6,
        # "CntsHigherThan14":  analyze.Contamination.CntsHigherThan14,
        "SKFCM_ASCT_NAS":  analyze.Contamination.CodeNas1638,
        # "SKFCM_ASCT_Partgt4":  analyze.Contamination.ParticlesHigherThan4,
        # "SKFCM_ASCT_Partgt6":  analyze.Contamination.ParticlesHigherThan6,
        # "SKFCM_ASCT_Partgt14":  analyze.Contamination.ParticlesHigherThan14,
        # "SKFCM_ASCT_Partgt21":  analyze.Contamination.ParticlesHigherThan21,
        # "SKFCM_ASCT_Partgt38":  analyze.Contamination.ParticlesHigherThan38, <-----------
        # "CutHigherThan20u":  analyze.Contamination.CutHigherThan20u,
        # "SlippingHigherThan20u":  analyze.Contamination.SlippingHigherThan20u,
        # "AbsPer01MmOfFreeWater":  analyze.Contamination.AbsPer01MmOfFreeWater,
        # "Bubbles":  analyze.Contamination.Bubbles,
        "SKFCM_ASCT_KFPpm":  analyze.Contamination.PpmWater,
        "SKFCM_ASCT_PcFerLarge":  analyze.Contamination.PercentageFeHigh,
        "SKFCM_ASCT_Silicon":  analyze.Contamination.Silicon,

        #Wear
        "SKFCM_ASCT_Iron":  analyze.Wear.Iron,
        "SKFCM_ASCT_Chromium":  analyze.Wear.Chromium,
        "SKFCM_ASCT_Nickel":  analyze.Wear.Nickel,
        "SKFCM_ASCT_Aluminum":  analyze.Wear.Aluminum,
        "SKFCM_ASCT_Lead":  analyze.Wear.LeadMetal,
        "SKFCM_ASCT_Copper":  analyze.Wear.Copper,
        "SKFCM_ASCT_Tin":  analyze.Wear.Tin,
        "SKFCM_ASCT_Titanium":  analyze.Wear.Titanium,
        "SKFCM_ASCT_Silver":  analyze.Wear.Silver,
        "SKFCM_ASCT_Antimony":  analyze.Wear.Antimony,
        "SKFCM_ASCT_Cadmium":  analyze.Wear.Cadmium,
        # "SKFCM_ASCT_Manganese":  analyze.Wear.Manganese, <----------
        # "Bismuth":  analyze.Wear.Bismuth,
        # "Arsenic":  analyze.Wear.Arsenic,
        # "Indian":  analyze.Wear.Indian,
        # "Cobalt":  analyze.Wear.Cobalt,
        # "SKFCM_ASCT_Zirconium":  analyze.Wear.Zirconium, <---------
        # "SKFCM_ASCT_Tungsten":  analyze.Wear.Tungsten, <----------
        # "Cerium":  analyze.Wear.Cerium,
        # "Fatigue":  analyze.Wear.Fatigue,
        # "NonMetalic":  analyze.Wear.NonMetalic,
        # "PpmFeHigh":  analyze.Wear.PpmFeHigh,
        # "FeWearSeverityIndex":  analyze.Wear.FeWearSeverityIndex,
        # "SKFCM_ASCT_PartTotal":  analyze.Wear.TotalFe, <--------
    }