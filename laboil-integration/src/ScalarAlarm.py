import helpers

class ScalarAlarm(object): 
      
    def __init__(self, values_dict):

        self.ScalarAlrmId = values_dict["ScalarAlrmId"]
        self.EnableDangerHi = values_dict["EnableDangerHi"]
        self.EnableDangerLo = values_dict["EnableDangerLo"]
        self.EnableAlertHi = values_dict["EnableAlertHi"]
        self.EnableAlertLo = values_dict["EnableAlertLo"]
        self.AlarmMethod = values_dict["AlarmMethod"]
        self.DangerLo = values_dict["DangerLo"]
        self.DangerHi = values_dict["DangerHi"]
        self.AlertLo = values_dict["AlertLo"]
        self.AlertHi = values_dict["AlertHi"]