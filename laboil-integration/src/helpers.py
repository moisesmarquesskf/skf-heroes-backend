from datetime import datetime as dt
import json
import boto3

def set_class_property(instance, propertyName, valuesDict, required):
    
    propertyNameDict = propertyName[:1].lower() + propertyName[1:] if propertyName else ''
    
    if required and not contains_Key(valuesDict, propertyNameDict):
        raise AttributeError("Required property {0} not present in dictionary.".format(propertyName))
    else:
        return setattr(instance, propertyName, valuesDict[propertyNameDict])

def contains_Key(dict, key): 
    return key in dict.keys()

def get_datetime() -> str:
    return dt.now().strftime("%Y%m%d%H%M%S")

def row_to_dict(cursor, row):
    columns = [column[0] for column in cursor.description]
    return dict(zip(columns, row))

def dict_to_json_string(dict):
    json.dumps(dict, default=lambda o: o.__dict__, sort_keys=False, indent=4)
    
def get_parameter(parameter_name):
    client = boto3.client('ssm')
    response = client.get_parameter(
                Name=parameter_name,
                WithDecryption=True
                )
    return json.loads(response["Parameter"]["Value"])