import helpers

class Chemistry(object): 

    def __init__(self, values_dict):

        self.Molybdenum: float
        self.Calcium: float
        self.Magnesium: float
        self.Phosphor: float
        self.Zinc: float
        self.Barium: float
        self.Boron: float
        self.Tan: float
        self.Oxidation: float
        self.Viscosity40: float
        self.FluidIntegrity: float
        
        helpers.set_class_property(self, "Molybdenum", values_dict, True)
        helpers.set_class_property(self, "Calcium", values_dict, True)
        helpers.set_class_property(self, "Magnesium", values_dict, True)
        helpers.set_class_property(self, "Phosphor", values_dict, True)
        helpers.set_class_property(self, "Zinc", values_dict, True)
        helpers.set_class_property(self, "Barium", values_dict, True)
        helpers.set_class_property(self, "Boron", values_dict, True)
        helpers.set_class_property(self, "Tan", values_dict, True)
        helpers.set_class_property(self, "Oxidation", values_dict, True)
        helpers.set_class_property(self, "Viscosity40", values_dict, True)
        helpers.set_class_property(self, "FluidIntegrity", values_dict, True)
