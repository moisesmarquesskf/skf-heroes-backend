import helpers

class Wear(object): 
      
    def __init__(self, values_dict):
        
        self.Iron: float
        self.Chromium: float
        self.Nickel: float
        self.Aluminum: float
        self.LeadMetal: float
        self.Copper: float
        self.Tin: float
        self.Titanium: float
        self.Silver: float
        self.Antimony: float
        self.Cadmium: float
        self.Manganese: float
        self.Bismuth: float
        self.Arsenic: float
        self.Indian: float
        self.Cobalt: float
        self.Zirconium: float
        self.Tungsten: float
        self.Cerium: float
        self.Fatigue: float
        self.NonMetalic: float
        self.PpmFeHigh: float
        self.FeWearSeverityIndex: float
        self.TotalFe: float

        helpers.set_class_property(self, "Iron", values_dict, True)
        helpers.set_class_property(self, "Chromium", values_dict, True)
        helpers.set_class_property(self, "Nickel", values_dict, True)
        helpers.set_class_property(self, "Aluminum", values_dict, True)
        helpers.set_class_property(self, "LeadMetal", values_dict, True)
        helpers.set_class_property(self, "Copper", values_dict, True)
        helpers.set_class_property(self, "Tin", values_dict, True)
        helpers.set_class_property(self, "Titanium", values_dict, True)
        helpers.set_class_property(self, "Silver", values_dict, True)
        helpers.set_class_property(self, "Antimony", values_dict, True)
        helpers.set_class_property(self, "Cadmium", values_dict, True)
        helpers.set_class_property(self, "Manganese", values_dict, True)
        helpers.set_class_property(self, "Bismuth", values_dict, True)
        helpers.set_class_property(self, "Arsenic", values_dict, True)
        helpers.set_class_property(self, "Indian", values_dict, True)
        helpers.set_class_property(self, "Cobalt", values_dict, True)
        helpers.set_class_property(self, "Zirconium", values_dict, True)
        helpers.set_class_property(self, "Tungsten", values_dict, True)
        helpers.set_class_property(self, "Cerium", values_dict, True)
        helpers.set_class_property(self, "Fatigue", values_dict, True)
        helpers.set_class_property(self, "NonMetalic", values_dict, True)
        helpers.set_class_property(self, "PpmFeHigh", values_dict, True)
        helpers.set_class_property(self, "FeWearSeverityIndex", values_dict, True)
        helpers.set_class_property(self, "TotalFe", values_dict, True)

