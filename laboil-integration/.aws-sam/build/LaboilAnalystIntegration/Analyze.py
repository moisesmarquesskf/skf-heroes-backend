import helpers
from Chemistry import Chemistry
from Contamination import Contamination
from Wear import Wear

class Analyze(object): 
      
    def __init__(self, values_dict):

        self.Id: int
        self.SampleId: int
        self.LocationDescription: str
        self.UnityId: str
        self.Model: str
        self.ComponentType: str
        self.OilDescription: str
        self.CollectDate: str
        self.ReleaseDate: str
        self.AssetOperationTime: int
        self.OilOperationTime: int
        self.Comments: str
        self.Diagnostics: str
        self.RecommendedActions: str
        self.AdditionalRecommendations: str

        helpers.set_class_property(self, "Id", values_dict, True)
        helpers.set_class_property(self, "SampleId", values_dict, True)
        helpers.set_class_property(self, "LocationDescription", values_dict, True)
        helpers.set_class_property(self, "UnityId", values_dict, True)
        helpers.set_class_property(self, "Model", values_dict, True)
        helpers.set_class_property(self, "ComponentType", values_dict, True)
        helpers.set_class_property(self, "OilDescription", values_dict, True)
        helpers.set_class_property(self, "CollectedDate", values_dict, True)
        helpers.set_class_property(self, "ReleaseDate", values_dict, True)
        helpers.set_class_property(self, "AssetOperationTime", values_dict, True)
        helpers.set_class_property(self, "OilOperationTime", values_dict, True)
        helpers.set_class_property(self, "Comments", values_dict, True)
        helpers.set_class_property(self, "Diagnostics", values_dict, True)
        helpers.set_class_property(self, "RecommendedActions", values_dict, True)
        helpers.set_class_property(self, "AdditionalRecommendations", values_dict, True)

        self.Chemistry = Chemistry(values_dict["Chemistry"])
        self.Contamination = Contamination(values_dict["Contamination"])
        self.Wear = Wear(values_dict["Wear"])

