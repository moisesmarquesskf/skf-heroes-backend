'''
Payload from SQS

{
    "Records": [
        {
            "messageId": "3c8bf4dc-360e-4fe1-9e09-8ca1081aa6b3",
            "receiptHandle": "AQEBM7oyRgJOLcjXhsSjxuz559BbwRjCRWAvHtdjmE01RbXkKHQOyuVfcRXvUyn/iGkQDxmQF2q8Tqgjm/2ZsqXa2EJ6OviTzw8dov/IlEaXJZAy3pLy555UClTZmIIShhe0oWStELJ4UJ6vAX9/oMWmfOBe8JQDgtHNUmmwxuip/QtxC+eh+zwV/VEpoRd2yI7fKkl+/yxbTk7Kr/08LosXthWM0Z3cJdqMjSidS6KntYO2aVFMR7aqRCV1hYJZrTBr29iS83cSOaBiflyuyXIXZKE8N+guk0lf8F7xhaIBXkqhk0ID+S8wuoRxzX65PHjIJ8vA6kRz4Tjdz5HFkKNNsWTR+LCPVaHc8qDLjQQ0cursQgVlGy/0PN8gkZZeT9kGGtfAsO/Zl6FYTfSLt9JdQEwYRzKfVnLn8w4GLvQpf70=",
            "body": "{\"customerShortname\":\"USDEMO01\",\"customerName\":\"USDEMO\",\"analyzes\":[{\"id\":42984,\"sampleId\":\"5933794-3776\",\"identifierShortname\":\"BRAEO1901\",\"locationDescription\":\"Local\",\"unityId\":\"ml\",\"model\":\"model\",\"componentType\":\"redutor\",\"oilDescription\":\"oleo desc\",\"collectedDate\":\"2021-02-10T16:44:16.952Z\",\"releaseDate\":\"2021-02-10T16:44:16.952Z\",\"assetOperationTime\":50,\"oilOperationTime\":50,\"comments\":\"comentario\",\"diagnostics\":\"normal\",\"recommendedActions\":\"ok\",\"additionalRecommendations\":\"ok\",\"chemistry\":{\"molybdenum\":50.5,\"calcium\":50.5,\"magnesium\":50.5,\"phosphor\":50.5,\"zinc\":50.5,\"barium\":50.5,\"boron\":50.5,\"bronze\":50.5,\"oxidation\":50.5,\"viscosity40\":50.5,\"fluidIntegrity\":50.5},\"contamination\":{\"sodium\":50.5,\"banadium\":50.5,\"potassium\":50.5,\"lithium\":50.5,\"codeIso4406HigherThan4\":50.5,\"codeIso4406HigherThan6\":50.5,\"codeIso4406HigherThan14\":50.5,\"cntsHigherThan4\":50.5,\"cntsHigherThan6\":50.5,\"cntsHigherThan14\":50.5,\"codeNas1638\":50.5,\"particlesHigherThan4\":50.5,\"particlesHigherThan6\":50.5,\"particlesHigherThan14\":50.5,\"particlesHigherThan21\":50.5,\"particlesHigherThan38\":50.5,\"cutHigherThan20u\":50.5,\"slippingHigherThan20u\":50.5,\"absPer01MmOfFreeWater\":50.5,\"bubbles\":50.5,\"ppmWater\":50.5,\"percentageFeHigh\":50.5,\"silicon\":0},\"wear\":{\"iron\":50.5,\"chromium\":50.5,\"nickel\":50.5,\"aluminum\":50.5,\"leadMetal\":50.5,\"copper\":50.5,\"tin\":50.5,\"titanium\":50.5,\"silver\":50.5,\"antimony\":50.5,\"cadmium\":50.5,\"manganese\":50.5,\"bismuth\":50.5,\"arsenic\":50.5,\"indian\":50.5,\"cobalt\":50.5,\"zirconium\":50.5,\"tungsten\":50.5,\"cerium\":50.5,\"fatigue\":50.5,\"nonMetalic\":50.5,\"ppmFeHigh\":50.5,\"feWearSeverityIndex\":50.5,\"totalFe\":50.5}}]}",
            "attributes": {
              "ApproximateReceiveCount": "1",
              "SentTimestamp": "1617036682200",
              "SenderId": "AROAW3ULQ5Y52TIWA5EDL:guilherme.b.rodrigues@skf.com",
              "ApproximateFirstReceiveTimestamp": "1617036682207"
          },
          "messageAttributes": {},
          "md5OfBody": "79bbe587f9c5c540c8c6a7eb0bb30002",
          "eventSource": "aws:sqs",
          "eventSourceARN": "arn:aws:sqs:us-east-1:471665274427:skf-oil-analysis-integration",
          "awsRegion": "us-east-1"
        }
    ]
}
'''

import os
import json
import boto3
import helpers
import pyodbc
import AnalyzeMap
from CustomerAnalyze import CustomerAnalyze
from ScalarAlarm import ScalarAlarm


def lambda_handler(event, context):
  
    for record in event["Records"]:
        data = json.loads(record["body"])
    
        try:
            customer_analyze = CustomerAnalyze(data)
            
            print("ShortName: ", customer_analyze.CustomerShortname)
            
            insert_analyzes(customer_analyze)
    
        except Exception as e:
            print(e)
            next
    
    
def insert_analyzes(customer_analyze: CustomerAnalyze):

    connection_params = get_client_config(customer_analyze.CustomerShortname.lower())
    
    if len(connection_params) == 0:
        raise Exception("Error: configuration not found for " + customer_analyze.CustomerShortname)
    
    connection_string = get_connection_string(connection_params[0])

    for analyze in customer_analyze.Analyzes:
        try:
            
            print("Location:", analyze.LocationDescription)
            
            analyze_map = AnalyzeMap.getMap(analyze)    
            
            for signature, overall_value in analyze_map.items():
                try:
                    insert_point(connection_string, analyze, signature, overall_value)
                except Exception as e:
                    print(e)
                    next
                
        except Exception as e:
            print(e)
            next
                


def insert_point(connection_string, analyze, signature, overall_value):
    
    connection = pyodbc.connect(connection_string)
    
    with connection:
        
        cursor = connection.cursor()

        insert_new(cursor, overall_value, analyze.Model, signature)
        
        connection.commit()


def get_client_config(shortName):
    lam = boto3.client('lambda')
    
    payload = {
        'region': 'us-east-1',
        'shortName': shortName
    }
    
    response = lam.invoke(FunctionName='get-analyst-connection-parameters',
            InvocationType='RequestResponse',
            Payload=json.dumps(payload))
            
    result = response['Payload'].read()
    
    return json.loads(result)
    
    
def get_connection_string(connection_params):
    return 'DRIVER={ODBC Driver 17 for SQL Server};' + """SERVER={0};DATABASE={1};UID={2};PWD={3}""".format(
        connection_params['AptitudeIP'],
        connection_params['ShortName'],
        connection_params['AptitudeUsuario'],
        connection_params['AptitudeSenha'] )


def insert_new(cursor, overall_value, valuestring, signature):

    print("Inserting point:", overall_value, valuestring, signature)

    sql_query = """
        SET NOCOUNT ON;

        DECLARE

        @REGISTRATION_ID AS INT,
        @MEASUREMENT_ID AS INT,
        @MEASREADING_ID AS INT,
        @POINT_ID AS INT,
        @ALARM_ID AS INT,
        @ALARM_LEVEL AS INT,
        @RESULT_TYPE AS INT,
        @OVERALL_VALUE AS DECIMAL(18,2) = {0}


        SET @REGISTRATION_ID = (SELECT REGISTRATIONID FROM skfuser1.REGISTRATION WHERE SIGNATURE = 'SKFCM_ASMD_Overall');

        IF @REGISTRATION_ID IS NULL
            THROW 51000, 'Registration not found', 1;

        SET @POINT_ID = (select TOP 1 
            P1.ELEMENTID AS POINTID
            from skfuser1.POINT P1 
            inner join 
                skfuser1.REGISTRATION R1 on P1.FIELDID=R1.REGISTRATIONID AND R1.SIGNATURE='SKFCM_ASPF_Point_Tag'
            left join 
                skfuser1.POINT P2 on P2.ELEMENTID = P1.ELEMENTID 
            inner join  
                skfuser1.REGISTRATION R2 on P2.FIELDID=R2.REGISTRATIONID AND R2.SIGNATURE='SKFCM_ASPF_Contaminent_Id'
            left join 
                skfuser1.REGISTRATION R3 on cast(R3.REGISTRATIONID as varchar) = P2.VALUESTRING 
            inner join
                skfuser1.TREEELEM T1 on P1.ELEMENTID = T1.TREEELEMID
            where 
                P1.VALUESTRING = ?
                and R3.SIGNATURE = ?
                and T1.PARENTID <> 2147000000
                and T1.ELEMENTENABLE = 1
        and T1.PARENTENABLE = 0
        and T1.HIERARCHYTYPE=1);

        IF @POINT_ID IS NULL
            THROW 51000, 'Point not found', 1;

        INSERT INTO skfuser1.measurement with (holdlock)
            (
                pointid, 
                alarmlevel, 
                status,
                include,
                opername,
                serialno,
                datadtg,
                MeasurementType
            )
            VALUES ( @POINT_ID, 0, 1, 1, 'Admin', 'Manual', format(getdate(),'yyyyMMddHHmmss'), 0 );
                    
        SET @MEASUREMENT_ID = SCOPE_IDENTITY();

        INSERT INTO  skfuser1.measreading   
            (
                measid , 
                pointid , 
                readingtype , 
                channel , 
                overallvalue , 
                exdwordval1 , 
                exdoubleval1 , 
                exdoubleval2 , 
                exdoubleval3 , 
                readingheader , 
                readingdata
            )
            VALUES ( @MEASUREMENT_ID, @POINT_ID, @REGISTRATION_ID, 1, @OVERALL_VALUE, 0, 0, 0, 0, NULL, NULL );

        SET @MEASREADING_ID = SCOPE_IDENTITY();

        DELETE FROM skfuser1.MeasAlarm
            WHERE PointId=@POINT_ID
            AND AlarmType=@REGISTRATION_ID
            AND TableName='ScalarAlarm';


        SET @ALARM_ID = (SELECT ISNULL( 
            (SELECT AlarmId FROM skfuser1.AlarmAssign WHERE ElementId = @POINT_ID AND Type = @REGISTRATION_ID),
            (SELECT ScalarAlrmId FROM skfuser1.ScalarAlarm WHERE ElementId = @POINT_ID)));

        IF @ALARM_ID IS NULL
            THROW 51000, 'Alarm not found', 1;

        SET @ALARM_LEVEL = (SELECT case 
                when AlarmMethod = 1 then
                    case 
                        when EnableDangerHi = 1 and @OVERALL_VALUE >= DangerHi then 4
                        when EnableAlertHi = 1 and @OVERALL_VALUE >= AlertHi then 3
                    end
                when AlarmMethod = 2 then
                    case
                        when EnableAlertHi = 1 and @OVERALL_VALUE <= AlertHi then 3
                        when EnableAlertLo = 1 and @OVERALL_VALUE >= AlertLo then 3
                        when EnableDangerHi = 1 and @OVERALL_VALUE <= DangerHi then 4
                        when EnableDangerLo = 1 and @OVERALL_VALUE >= DangerLo then 4
                    end
                when AlarmMethod = 3 then
                    case
                        when EnableDangerLo = 1 and @OVERALL_VALUE <= DangerLo then 4
                        when EnableDangerHi = 1 and @OVERALL_VALUE >= DangerHi then 4
                        when EnableAlertLo = 1 and @OVERALL_VALUE <= AlertLo then 3
                        when EnableAlertHi = 1 and @OVERALL_VALUE >= AlertHi then 3
                    end
                end as AlarmLevel
            from skfuser1.ScalarAlarm 
            WHERE
                ScalarAlrmId = @ALARM_ID);

        SET @RESULT_TYPE = (SELECT case 
                    when AlarmMethod = 1 then
                        case 
                            when EnableDangerHi = 1 and @OVERALL_VALUE >= DangerHi then 3
                            when EnableAlertHi = 1 and @OVERALL_VALUE >= AlertHi then 1
                        end
                    when AlarmMethod = 2 then
                        case
                            when EnableAlertHi = 1 and @OVERALL_VALUE <= AlertHi then 1
                            when EnableAlertLo = 1 and @OVERALL_VALUE >= AlertLo then 2
                            when EnableDangerHi = 1 and @OVERALL_VALUE <= DangerHi then 3
                            when EnableDangerLo = 1 and @OVERALL_VALUE >= DangerLo then 4
                        end
                    when AlarmMethod = 3 then
                        case
                            when EnableDangerLo = 1 and @OVERALL_VALUE <= DangerLo then 4
                            when EnableDangerHi = 1 and @OVERALL_VALUE >= DangerHi then 3
                            when EnableAlertLo = 1 and @OVERALL_VALUE <= AlertLo then 2
                            when EnableAlertHi = 1 and @OVERALL_VALUE >= AlertHi then 1
                        end
                end as ResultType
            from skfuser1.ScalarAlarm 
            WHERE
                ScalarAlrmId = @ALARM_ID);


        INSERT INTO skfuser1.MeasAlarm 
            ( 
                PointId ,
                ReadingId ,
                AlarmId ,
                AlarmType ,
                AlarmLevel ,
                ResultType ,
                TableName ,
                MeasId ,
                Channel ) 
            VALUES ( @POINT_ID, @MEASREADING_ID, @ALARM_ID, @REGISTRATION_ID, @ALARM_LEVEL, @RESULT_TYPE, 'ScalarAlarm', @MEASUREMENT_ID, 1 );
    
    
    """.format(overall_value)

    cursor.execute(sql_query, (valuestring, signature))
