import helpers

class Contamination(object): 
      
    def __init__(self, values_dict):

        self.Sodium: float
        self.Vanadium: float
        self.Potassium: float
        self.Lithium: float
        self.CodeIso4406HigherThan4: float
        self.CodeIso4406HigherThan6: float
        self.CodeIso4406HigherThan14: float
        self.CntsHigherThan4: float
        self.CntsHigherThan6: float
        self.CntsHigherThan14: float
        self.CodeNas1638: float
        self.ParticlesHigherThan4: float
        self.ParticlesHigherThan6: float
        self.ParticlesHigherThan14: float
        self.ParticlesHigherThan21: float
        self.ParticlesHigherThan38: float
        self.CutHigherThan20u: float
        self.SlippingHigherThan20u: float
        self.AbsPer01MmOfFreeWater: float
        self.Bubbles: float
        self.PpmWater: float
        self.PercentageFeHigh: float
        self.Silicon: float
        
        helpers.set_class_property(self, "Sodium", values_dict, True)
        helpers.set_class_property(self, "Vanadium", values_dict, True)
        helpers.set_class_property(self, "Potassium", values_dict, True)
        helpers.set_class_property(self, "Lithium", values_dict, True)
        helpers.set_class_property(self, "CodeIso4406HigherThan4", values_dict, True)
        helpers.set_class_property(self, "CodeIso4406HigherThan6", values_dict, True)
        helpers.set_class_property(self, "CodeIso4406HigherThan14", values_dict, True)
        helpers.set_class_property(self, "CntsHigherThan4", values_dict, True)
        helpers.set_class_property(self, "CntsHigherThan6", values_dict, True)
        helpers.set_class_property(self, "CntsHigherThan14", values_dict, True)
        helpers.set_class_property(self, "CodeNas1638", values_dict, True)
        helpers.set_class_property(self, "ParticlesHigherThan4", values_dict, True)
        helpers.set_class_property(self, "ParticlesHigherThan6", values_dict, True)
        helpers.set_class_property(self, "ParticlesHigherThan14", values_dict, True)
        helpers.set_class_property(self, "ParticlesHigherThan21", values_dict, True)
        helpers.set_class_property(self, "ParticlesHigherThan38", values_dict, True)
        helpers.set_class_property(self, "CutHigherThan20u", values_dict, True)
        helpers.set_class_property(self, "SlippingHigherThan20u", values_dict, True)
        helpers.set_class_property(self, "AbsPer01MmOfFreeWater", values_dict, True)
        helpers.set_class_property(self, "Bubbles", values_dict, True)
        helpers.set_class_property(self, "PpmWater", values_dict, True)
        helpers.set_class_property(self, "PercentageFeHigh", values_dict, True)
        helpers.set_class_property(self, "Silicon", values_dict, True)









