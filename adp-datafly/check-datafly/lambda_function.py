import json
import boto3
import functools
import uuid
from datetime import datetime
from datetime import timedelta
from boto3.dynamodb.conditions import Key, Attr
import os
sqs = boto3.client('sqs')

def lambda_handler(event, context):
    clients =  get_next_run()
    
    for client in clients:
        if client["lastrun"] == "" or timedelta.total_seconds(datetime.now() - datetime.fromisoformat(client["lastrun"]))/60 >= client["runeveryminutes"]:
            
            queue_url = os.environ['GET_DATAFLY_ASSETS_SQS']
            
            # Send message to SQS queue
            response = sqs.send_message(
                QueueUrl=queue_url,
                DelaySeconds=0,
                MessageAttributes={},
                MessageBody=(json.dumps({
                    "shortName": client["shortname"]
                }))
            )
            
            print("Message created: {0}".format(response['MessageId']))
            
            put_next_run(client["shortname"], client["runeveryminutes"])


def get_next_run():
    
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
    table = dynamodb.Table('datafly_client_schedule')
    
    response = table.scan(
        FilterExpression=Attr('enabled').eq(True)
    )
    
    return response['Items']
    


def put_next_run(shortname, runeveryminutes):
    
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
    table = dynamodb.Table('datafly_client_schedule')
    
    #delete if exists
    table.delete_item(
        Key={
            'shortname': shortname
        }
    )
    
    response = table.put_item(
       Item= {
            'shortname': shortname,
            'lastrun': str(datetime.now()),
            'runeveryminutes': runeveryminutes,
            'enabled': True
        }
    )