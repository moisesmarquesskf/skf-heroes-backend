/* Request Payload
{
    parameterType: string "secret" | "encrypted" | default = null,
    parameterName: string,
    parameterRegion: string // aws region
}
*/

const { SSM, SecretsManager } = require("aws-sdk");

exports.handler = async function (event) {

    if(event.parameterType == 'secret')
        return await getSecret(event.parameterName, event.parameterRegion).then( res => JSON.parse(res));
    else if(event.parameterType == 'encrypted')
        return await getEncryptedParameter(event.parameterName).then( res => JSON.parse(res));
    else
        return await getParameter(event.parameterName).then( res => JSON.parse(res));
}

async function getParameterWorker (name, decrypt) {
    const ssm = new SSM();
    const result = await ssm
    .getParameter({ Name: name, WithDecryption: decrypt })
    .promise();
    return result.Parameter.Value;
}

async function getParameter (name) {
    return getParameterWorker(name, false);
}

async function getEncryptedParameter (name) {
    return getParameterWorker(name, true);
}

async function getSecret(secretName, region) {
    
    return new Promise((resolve, reject) => {
        
        var client = new SecretsManager({
            region: region
        });
        
        return client.getSecretValue({ SecretId: secretName }, function(err, data) {
            if (err) {
                reject(err)
            }
            else {
                // Decrypts secret using the associated KMS CMK.
                // Depending on whether the secret is a string or binary, one of these fields will be populated.
                if ('SecretString' in data) {
                    resolve(data.SecretString)
                } else {
                    let buff = new Buffer(data.SecretBinary, 'base64');
                    resolve(buff.toString('ascii'))
                }
            }
        })
        
    });
}
