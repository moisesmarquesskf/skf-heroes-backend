/* Request Payload
{
    shortName: string,
    region: string ***aws_region***
}
*/
const { Lambda } = require("aws-sdk");

const mssql = require('mssql');

exports.handler = async function (event) {

    const lambda = new Lambda({ region: event.region });

    const sqlConfig = await new Promise((resolve, reject) => lambda.invoke({
        FunctionName: 'get-parameter',
        Payload: JSON.stringify({ 
            "parameterName": "/applications/repcenter/db/rdcinternal",
            "parameterType": "encrypted",
            "parameterRegion": event.region 
        })
      }, (err, data) => (err) ? reject(err, err.stack) : resolve(JSON.parse(data.Payload))));
      
    await mssql.connect({
        user: sqlConfig.user_name,
        password: sqlConfig.user_password,
        database: sqlConfig.database_name,
        server: sqlConfig.host_name,
        pool: {
            max: 10,
            min: 0,
            idleTimeoutMillis: 30000
        },
        options: {
            encrypt: true,
            trustServerCertificate: true
        }
    });

    const result = await mssql.query`
        SELECT	a.ShortName
            , b.ShortName as BranchName
            , b.TblSetId
            , a.LongName
            , a.AptitudeIP
            , a.AptitudeSenha
            , a.AptitudeUsuario 
            , a.SAMIP
            , a.SAMUsuario
            , a.SAMSenha
            , a.SAMPath
        FROM SKFAdmin.RDCCustomer a 
        INNER JOIN SKFAdmin.RDCCustomer_detail b 
        ON a.shortname = b.maincustomer 
    WHERE B.shortname LIKE ${ event.shortName }
    GROUP BY a.ShortName
            , b.ShortName
            , b.TblSetId
            , a.LongName
            , a.AptitudeIP
            , a.AptitudeSenha
            , a.AptitudeUsuario 
            , a.SAMIP
            , a.SAMUsuario
            , a.SAMSenha
            , a.SAMPath 
    ORDER BY 1`;

    return result.recordset;
}
